function Calculate() {
var c1 = parseInt(document.getElementById('cr1').value);
var c2 = parseInt(document.getElementById('cr2').value);
var total = c1+c2;
 document.getElementById("totalCredits").innerHTML = total;
 // Compute GPA here
 var gradePoints1;
 var gradePoints2;
gradePoints1 = letterToPoints(document.getElementById('g1').value);

gradePoints1 = gradePoints1 * c1;

gradePoints2 = letterToPoints(document.getElementById('g2').value);
gradePoints2 = gradePoints2 * c2;
 var gpa = (gradePoints1+gradePoints2)/total
  document.getElementById("gpa").innerHTML = gpa;
 // print GPA
};
function letterToPoints(grade) {
    if (grade=='A+')
    return 4;
 else if(grade=='A')
    return 3.75;
 else if(grade=='B+')
    return 3.5;
 else if(grade=='B')
    return 3;
 else if(grade=='C+')
    return 2.5;
 else if(grade=='C')
    return 2;
 else if(grade=='D+')
    return 1.5;
 else if(grade='D')
    return 1;
else
    return 0;
}